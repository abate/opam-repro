#!/bin/bash

# this script check for reproducibility.
# it's invoked by opam from the post-install script
# add
#    post-install-commands: [
#        "%{hooks}%/opamrepro.sh" "%{name}%" "%{version}%" "%{installed-files}%"
#    ]
# in ~/.opam/config
#
# logs in the file /tmp/opamrepro/log
# at the moment, the script always succeds

NAME=$1
VERSION=$2
shift 2
FILES=$*

mkdir -p /tmp/opamrepro
file=/tmp/opamrepro/${NAME}.${VERSION}
log=/tmp/opamrepro/log

#there is no artifact to check
if [ -z "$FILES" ]; then
  exit 0
fi

code=0
for i in $FILES; do
  if ! [[ $i == *.cmt* ]]; then
    check=$OPAM_SWITCH_PREFIX/$i
    if [ -f "$file" ] ; then
      grep -q "$check" "$file"
      code=$?
    else
      code=-1
    fi
  #else
    #echo "Skip $i" >> "$log"
  fi
done

if [ $code -eq 0 ]; then
  MD5LOG=$(md5sum -c "$file")
  err=$?
  if [ $err -eq 0 ]; then
    echo "$NAME.$VERSION is reproductible" >> "$log"
  else
    echo "$NAME.$VERSION is NOT reproductible" >> "$log"
    echo "$MD5LOG" >> "$log"
    #exit 1
  fi
else
  echo "Generating md5sum for $NAME.$VERSION ==" >> "$log"
  for i in $FILES; do
    if ! [[ $i == *.cmt* ]]; then
      check=$OPAM_SWITCH_PREFIX/$i
      md5sum "$check" >> "$file"
    fi
  done
fi
